<?php

namespace Drupal\tckk_field;

/**
 * TckkValidator service.
 *
 * Validates T.C. Kimlik no.
 */
class TckkValidator {

  /**
   * {@inheritdoc}
   */
  public function validate(string $tc_kimlik) {
    $tc_kimlik = preg_replace('/[^0-9]/', '', $tc_kimlik);
    if (\strlen($tc_kimlik) !== 11 || $tc_kimlik[0] === 0 || $this->sum($tc_kimlik) || $this->on($tc_kimlik)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function sum(string $tc_kimlik) {
    $even   = ((int) $tc_kimlik[0]) + ((int) $tc_kimlik[2]) + ((int) $tc_kimlik[4]) + ((int) $tc_kimlik[6]) + ((int) $tc_kimlik[8]);
    $odd    = ((int) $tc_kimlik[1]) + ((int) $tc_kimlik[3]) + ((int) $tc_kimlik[5]) + ((int) $tc_kimlik[7]);
    $even  *= 7;
    $result = \abs($even - $odd);
    if ($result % 10 !== ((int) $tc_kimlik[9])) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function on(string $tc_kimlik) {
    $sum = 0;
    for ($i = 0; $i < 10; $i++) {
      $sum += ((int) $tc_kimlik[$i]);
    }
    if ($sum % 10 !== ((int) $tc_kimlik[10])) {
      return TRUE;
    }

    return FALSE;
  }

}
