<?php

namespace Drupal\tckk_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'tckk_field_default' formatter.
 *
 * @FieldFormatter(
 *   id = "tckk_field_default",
 *   label = @Translation("Default"),
 *   field_types = {"tckk_field"}
 * )
 */
class TCKimlikNoDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      if ($item->tckk_field) {
        $element[$delta]['tckk_field'] = [
          '#type'   => 'item',
          '#markup' => $item->tckk_field,
        ];
      }

    }

    return $element;
  }

}
