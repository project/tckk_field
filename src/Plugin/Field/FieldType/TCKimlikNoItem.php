<?php

namespace Drupal\tckk_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'tckk_field' field type.
 *
 * @FieldType(
 *   id = "tckk_field",
 *   label = @Translation("T.C. Kimlik No"),
 *   category = @Translation("General"),
 *   default_widget = "tckk_field",
 *   default_formatter = "tckk_field_default"
 * )
 */
class TCKimlikNoItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->tckk_field !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['tckk_field'] = DataDefinition::create('string');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'tckk_field' => [
        'type' => 'varchar',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      // @DCG Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {

    $scale = rand(10, 2);
    $random_decimal = -1000 + mt_rand() / mt_getrandmax() * (-1000 - 1000);
    $values['tckk_field'] = floor($random_decimal * pow(10, $scale)) / pow(10, $scale);

    return $values;
  }

}
