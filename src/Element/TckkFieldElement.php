<?php

namespace Drupal\tckk_field\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a tckk field element.
 *
 * @FormElement("tckk")
 */
class TckkFieldElement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = \get_class($this);
    return [
      '#input' => TRUE,
      '#element_validate' => [
        [$class, 'validateTckk'],
      ],
      '#process' => [
        [$class, 'processTckk'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderTck'],
        [$class, 'preRenderGroup'],
      ],
      '#theme'          => 'input__textfield',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function processTckk(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['tckk'] = [
      '#name'             => $element['#name'],
      '#title'            => t('T.C. Kimlik No'),
      '#title_display'    => 'invisible',
      '#default_value'    => \array_key_exists('#default_value', $element) ? $element['#default_value'] : '',
      '#attributes'       => $element['#attributes'],
      '#required'         => $element['#required'],
      '#error_no_message' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public static function preRenderTck($element) {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, ['id', 'name', 'value']);
    static::setAttributes($element, ['form-text']);
    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public static function validateTckk(&$element, FormStateInterface $form_state, &$complete_form) {
    /** @var \Drupal\tckk_field\TckkValidator */
    $validator = \Drupal::service('tckk_field.validator');
    $value     = $element['#value'];
    if ($value !== '' && $validator->validate($value) === FALSE) {
      $form_state->setError($element, t('T.C. Kimlik No is not valid'));
    }
  }

}
